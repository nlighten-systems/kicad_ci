.PHONY: all clean build docker_run


all: build

clean:
	rm -rf output

build:
	cd dockerbuild && docker build -t kicad_ci . 
	docker tag kicad_ci registry.gitlab.com/nlighten-systems/kicad_ci:kicad_ci

pushdockerimage: dockerbuild/Dockerfile
	docker login registry.gitlab.com
	docker push registry.gitlab.com/nlighten-systems/kicad_ci:kicad_ci

docker_run:
	docker run --rm -it -u "$(shell id -u):$(shell id -g)" -v "$(abspath .):/data" kicad_ci /bin/bash



